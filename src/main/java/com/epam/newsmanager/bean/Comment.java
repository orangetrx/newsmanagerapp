package com.epam.newsmanager.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Comment class is representing comment model for table COMMENTS in database
 * COMMENTS table fields are COM_ID;COM_NEWS_ID;COM_TEXT;COM_CREATION_DATE;
 */
public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    private long commentId;
    private long newsId;
    private String commentText;
    private Date creationDate;

    public long getNewsId() {
        return newsId;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (commentId != comment.commentId) return false;
        if (newsId != comment.newsId) return false;
        if (commentText != null ? !commentText.equals(comment.commentText) : comment.commentText != null) return false;
        return !(creationDate != null ? !creationDate.equals(comment.creationDate) : comment.creationDate != null);

    }

    @Override
    public int hashCode() {
        int result = (int) (commentId ^ (commentId >>> 32));
        result = 31 * result + (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }
}
