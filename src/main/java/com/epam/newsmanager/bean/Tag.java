package com.epam.newsmanager.bean;

import java.io.Serializable;

/**
 * Tag class is representing tag model for table TAG in database. TAG table
 * fields are TG_ID;TG_NAME;
 */
public class Tag implements Serializable {
    private static final long serialVersionUID = 1L;

    private long tagId;
    private String tagName;

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }
}
