package com.epam.newsmanager.exception;

/**
 * Wrapper for Exception class {@link Exception}
 */
public class ApplicationException extends Exception {
    private static final long serialVersionUID = 1L;
    private Exception exception;

    public ApplicationException(String message, Exception e) {
        super(message);
        exception = e;
    }

    public ApplicationException(String message) {
        super(message);
    }

    public Exception getException() {
        return exception;
    }
}
