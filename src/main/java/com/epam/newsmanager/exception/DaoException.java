package com.epam.newsmanager.exception;

import com.epam.newsmanager.exception.ApplicationException;

/**
 *
 * All exceptions that may arise in DAO layer(e.g SQLException's), are wrapped
 * by this custom exception.
 *
 */
public class DaoException extends ApplicationException {
    public DaoException(String message, Exception e) {
        super(message, e);
    }

    public DaoException(String message) {
        super(message);
    }
}
