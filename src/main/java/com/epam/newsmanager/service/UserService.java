package com.epam.newsmanager.service;

import com.epam.newsmanager.bean.User;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;

/**
 * UserService is a common interface for service layer, implementation of which
 * is using for operating USERS table in database;
 */
public interface UserService {
    /**
     * This method is using for creating new USER object and Roles in database.
     *
     * @param user
     *            USER object {@link User} that contains all filled fields.
     * @return If succeed - returns ID(Long value) in database of created USER,
     *         otherwise null;
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    Long addUser(User user) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for loading the USER object(row) from database if it
     * exists.
     *
     * @param idUser
     *            ID of searching USER.
     * @return USER object {@link User} with a specified USER ID. If it doesn't exists, returns null.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    User loadUser(long idUser) throws ServiceException, NoSuchEntityException;

    /**
     * This method is using for updating USER object in database.
     *
     * @param user
     *            USER object {@link User} that contains all filled fields.
     * @throws ServiceException
     *             this exception throws when on service layer (e.g.)
     *             DaoException caught.
     */
    void updateUser(User user) throws ServiceException;

}
