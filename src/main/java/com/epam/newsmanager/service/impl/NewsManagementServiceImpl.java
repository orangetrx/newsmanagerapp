package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import com.epam.newsmanager.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(rollbackFor = ServiceException.class)
public class NewsManagementServiceImpl implements NewsManagementService {


    @Autowired
    private NewsService newsService;

    @Autowired
    private TagService tagService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private CommentService commentService;

/**
 * Implementation of
 * {@link NewsManagementService#viewNewsTO(Long)} method
 */
    @Override
    public NewsTO viewNewsTO(Long newsId) throws ServiceException, NoSuchEntityException {
        News news = newsService.viewNews(newsId);
        List<Tag> tags = tagService.getTagsForNews(newsId);
        List<Comment> comments = commentService.loadCommentByNews(newsId);
        Author author = authorService.loadAuthorByNewsId(newsId);

        NewsTO newsTO = new NewsTO();
        newsTO.setComments(comments);
        newsTO.setNews(news);
        newsTO.setTags(tags);
        newsTO.setAuthor(author);

        return newsTO;
    }

    /**
     * Implementation of
     * {@link NewsManagementService#addNewsTO(NewsTO)} method
     */
    @Override
    public Long addNewsTO(NewsTO newsTO) throws ServiceException, NoSuchEntityException {
        Long createdNewsTOId = newsService.addNews(newsTO.getNews());

        if (createdNewsTOId != null) {
            for (Tag tag : newsTO.getTags()) {
                newsService.insertNewsTagLink(createdNewsTOId, tag.getTagId());
            }

            newsService.insertNewsAuthorLink(newsTO.getAuthor().getAuthorId(), createdNewsTOId);
        }

        if (createdNewsTOId == null) {
            throw new NoSuchEntityException("NewsTO was not added");
        }

        return createdNewsTOId;
    }

    /**
     * Implementation of
     * {@link NewsManagementService#deleteNews(Long)} method
     */
    @Override
    public void deleteNews(Long newsId) throws ServiceException {
        newsService.deleteNews(newsId);
        commentService.deleteCommentByNewsId(newsId);
    }

    /**
     * Implementation of
     * {@link NewsManagementService#viewAllNewsTO()} method
     */
    @Override
    public List<NewsTO> viewAllNewsTO() throws ServiceException, NoSuchEntityException {
        List<NewsTO> newsTOs = new ArrayList<>();
        for(News news : newsService.viewAllNews()) {
            newsTOs.add(viewNewsTO(news.getNewsId()));
        }

        //Collections.sort(newsTOs, new NewsTOCountCommentsComparator());

        return newsTOs;
    }
}
