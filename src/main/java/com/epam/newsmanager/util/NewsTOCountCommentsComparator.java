package com.epam.newsmanager.util;

import com.epam.newsmanager.bean.transferobject.NewsTO;

import java.util.Comparator;

/**
 * This is custom comparator class for NewsTO objects {@link NewsTO}
 * This comparator perform sort of collections with NewsTO objects by count of comments
 * in back order {@link NewsTO#comments}
 */
public class NewsTOCountCommentsComparator implements Comparator<NewsTO> {
    @Override
    public int compare(NewsTO o1, NewsTO o2) {
        return new Integer(o2.getComments().size()).compareTo(o1.getComments().size());
    }
}
