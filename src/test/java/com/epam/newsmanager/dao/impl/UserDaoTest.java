package com.epam.newsmanager.dao.impl;

import com.epam.newsmanager.bean.User;
import com.epam.newsmanager.dao.UserDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.util.DbTestUtil;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:test-app-contex.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup(value = "classpath:com/epam/newsmanager/dao/UserDaoTest.xml")
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Autowired
    private DataSource dataSource;

    @After
    public void reset() {
        DbTestUtil.resetAutoIncrementInUsers(dataSource);
    }

    @Test
    public void testCreate() throws DaoException {
        User expectedUser = new User();
        User actualUser;

        expectedUser.setLogin("login1");
        expectedUser.setUserName("user1");
        expectedUser.setPassword("password1");
        expectedUser.setUserId(4);
        long id = userDao.create(expectedUser);
        actualUser = userDao.read(id);

        Assert.assertEquals(expectedUser, actualUser);

    }

    @Test
    public void testRead() throws DaoException {
        User expectedUser = new User();
        User actualUser;

        expectedUser.setLogin("login1");
        expectedUser.setUserName("user1");
        expectedUser.setPassword("password1");
        expectedUser.setUserId(1);

        actualUser = userDao.read(new Long(1));

        Assert.assertEquals(expectedUser, actualUser);
    }

    @Test
    public void testUpdate() throws DaoException {
        User expectedUser = new User();
        expectedUser.setUserId(1);
        expectedUser.setLogin("newlogin");
        expectedUser.setPassword("newpass");
        expectedUser.setUserName("newusername");
        userDao.update(expectedUser);

        User actualUser = userDao.read((long) 1);

        Assert.assertEquals(actualUser, expectedUser);
    }

    @Test
    public void testDelete() throws DaoException {
        User actualUser;
        long dummyId = 1;
        userDao.delete(dummyId);
        actualUser = userDao.read(dummyId);

        Assert.assertNull(actualUser);
    }
}
