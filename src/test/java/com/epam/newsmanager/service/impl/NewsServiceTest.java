package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Author;
import com.epam.newsmanager.bean.Comment;
import com.epam.newsmanager.bean.News;
import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.bean.transferobject.NewsTO;
import com.epam.newsmanager.bean.transferobject.SearchCriteria;
import com.epam.newsmanager.dao.impl.OracleNewsDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class NewsServiceTest {

    @InjectMocks
    private NewsServiceImpl newsService;

    @Mock
    private OracleNewsDao newsDao;

    @Before
     public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddNews() throws DaoException, ServiceException, NoSuchEntityException {
        NewsTO dummyNewsTo = new NewsTO();
        long dummyNewsId = 1;
        News dummyNews = new News();
        dummyNews.setCreationDate(new Date(new Date().getTime()));
        dummyNews.setFullText("dummy full text");
        dummyNews.setNewsId(dummyNewsId);
        dummyNews.setModificationDate(new Date(new Date().getTime()));
        dummyNews.setShortText("dummy short text");
        dummyNews.setTitle("dummy short title");

        Author dummyAuthor = new Author();
        dummyAuthor.setAuthorName("dummy author name");
        dummyAuthor.setAuthorId(1);
        dummyAuthor.setExpired(new Date(new Date().getTime()));

        final Tag dummyTag = new Tag();
        dummyTag.setTagId(1);
        dummyTag.setTagName("dummy tag name");

        final Comment dummyComment = new Comment();
        dummyComment.setCommentId(1);
        dummyComment.setCreationDate(new Date(new Date().getTime()));
        dummyComment.setCommentText("dummy comment text");
        dummyComment.setNewsId(dummyNewsId);

        dummyNewsTo.setAuthor(dummyAuthor);
        dummyNewsTo.setTags(new ArrayList<Tag>() {{
            add(dummyTag);
        }});
        dummyNewsTo.setComments(new ArrayList<Comment>(){{add(dummyComment);}});
        dummyNewsTo.setNews(dummyNews);


        when(newsService.addNews(dummyNewsTo.getNews())).thenReturn(dummyNewsId);

        long actualNewsId = newsService.addNews(dummyNewsTo.getNews());
        assertEquals(dummyNewsId, actualNewsId);
        verify(newsDao, atLeastOnce()).create(dummyNewsTo.getNews());
    }

    @Test
    public void testEditNews() throws DaoException, ServiceException {
        News dummyNews = new News();
        newsService.editNews(dummyNews);
        verify(newsDao, atLeastOnce()).update(dummyNews);
    }

    @Test(expected = NoSuchEntityException.class)
    public void testViewNews() throws ServiceException, DaoException, NoSuchEntityException {
        News expectedNewsTo;
        long dummyNewsId = 1;

        News dummyNews = new News();
        dummyNews.setCreationDate(new Date(new Date().getTime()));
        dummyNews.setFullText("dummy full text");
        dummyNews.setNewsId(dummyNewsId);
        dummyNews.setModificationDate(new Date(new Date().getTime()));
        dummyNews.setShortText("dummy short text");
        dummyNews.setTitle("dummy short title");

        when(newsService.viewNews(dummyNewsId)).thenReturn(dummyNews);
        expectedNewsTo = newsService.viewNews(dummyNewsId);
        verify(newsDao, atLeastOnce()).read(dummyNewsId);
        assertEquals(expectedNewsTo, dummyNews);
    }

    @Test
    public void testViewAllNews() throws ServiceException, DaoException {
        List<News> dummyListOfNews = new ArrayList<>();
        List<News> expectedNewsList;

        when(newsService.viewAllNews()).thenReturn(dummyListOfNews);
        expectedNewsList = newsService.viewAllNews();

        verify(newsDao, atLeastOnce()).getAllNews();
        assertEquals(expectedNewsList, dummyListOfNews);
    }

    @Test
    public void testDeleteNews() throws ServiceException, DaoException {
        long dummyNewsId = 1;
        newsService.deleteNews(dummyNewsId);
        verify(newsDao, atLeastOnce()).delete(dummyNewsId);
    }

    @Test
    public void testSearchNews() throws ServiceException {
        SearchCriteria dummySearchCriteria = new SearchCriteria();
        dummySearchCriteria.setAuthorId(1);
        dummySearchCriteria.setTagIdList(new ArrayList<Long>(){{add(new Long(1));add(new Long(2));}});

        List<News> dummyListOfNews = new ArrayList<>();
        List<News> expectedListOfNews;

        when(newsService.searchNews(dummySearchCriteria)).thenReturn(dummyListOfNews);

        expectedListOfNews = newsService.searchNews(dummySearchCriteria);

        assertEquals(dummyListOfNews, expectedListOfNews);
    }


}
