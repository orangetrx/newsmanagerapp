package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.Tag;
import com.epam.newsmanager.dao.impl.OracleTagDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class TagServiceTest {

    @InjectMocks
    private TagServiceImpl tagService;

    @Mock
    private OracleTagDao tagDao;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddTag() throws ServiceException, DaoException {
        long dummyTagId = 1;
        Tag dummyTag = new Tag();
        when(tagService.addTag(dummyTag)).thenReturn(dummyTagId);

        long expectedTagId = tagService.addTag(dummyTag);
        assertEquals(expectedTagId, dummyTagId);
        verify(tagDao, atLeastOnce()).create(dummyTag);
    }

    @Test(expected = NoSuchEntityException.class)
    public void testGetTagById() throws ServiceException, DaoException, NoSuchEntityException {
        long dummyTagId = 1;
        Tag dummyTag = new Tag();
        Tag expectedTag;

        when(tagService.getTagById(dummyTagId)).thenReturn(dummyTag);
        expectedTag = tagService.getTagById(dummyTagId);

        assertEquals(expectedTag, dummyTag);
        verify(tagDao, atLeastOnce()).read(dummyTagId);
    }

    @Test
    public void testGetAllTags() throws ServiceException, DaoException {
        List<Tag> dummyListOfTags = new ArrayList<>();
        List<Tag> expectedListOfTags;

        when(tagService.getAllTags()).thenReturn(dummyListOfTags);
        expectedListOfTags = tagService.getAllTags();

        verify(tagDao, atLeastOnce()).getAllTags();
        assertEquals(expectedListOfTags, dummyListOfTags);
    }

    public void testGetTagsForNews() throws ServiceException, DaoException {
        long dummyNewsId = 1;
        List<Tag> dummyListOfTags = new ArrayList<>();
        List<Tag> expectedListOfTags;

        when(tagService.getTagsForNews(dummyNewsId)).thenReturn(dummyListOfTags);
        expectedListOfTags = tagService.getTagsForNews(dummyNewsId);

        verify(tagDao, atLeastOnce()).getTagsForNews(dummyNewsId);
        assertEquals(expectedListOfTags, dummyListOfTags);
    }

}
