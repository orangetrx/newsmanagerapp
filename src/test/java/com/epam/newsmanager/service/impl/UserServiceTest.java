package com.epam.newsmanager.service.impl;

import com.epam.newsmanager.bean.User;
import com.epam.newsmanager.dao.impl.OracleRoleDao;
import com.epam.newsmanager.dao.impl.OracleUserDao;
import com.epam.newsmanager.exception.DaoException;
import com.epam.newsmanager.exception.NoSuchEntityException;
import com.epam.newsmanager.exception.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.sql.DataSource;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private OracleUserDao userDao;

    @Mock
    private OracleRoleDao roleDao;

    @Mock
    private DataSource dataSource;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddUser() throws ServiceException, DaoException, NoSuchEntityException {
        User dummyUser = new User();
        Long dummyUserId = (long) 1;
        when(userService.addUser(dummyUser)).thenReturn(dummyUserId);

        Long actualId = userService.addUser(dummyUser);

        assertEquals(actualId, dummyUserId);
        verify(userDao, atLeastOnce()).create(dummyUser);
    }

    @Test(expected = NoSuchEntityException.class)
    public void testLoadUser() throws ServiceException, DaoException, NoSuchEntityException {
        long userId = 1;
        User dummyUser = new User();
        User expectedUser;

        when(userService.loadUser(userId)).thenReturn(dummyUser);
        expectedUser = userService.loadUser(userId);

        verify(userDao, atLeastOnce()).read(userId);
        assertEquals(expectedUser, dummyUser);
    }

    @Test
    public void testUpdateUser() throws ServiceException, DaoException {
        User dummyUser = new User();
        userService.updateUser(dummyUser);
        verify(userDao, atLeastOnce()).update(dummyUser);
    }


}
