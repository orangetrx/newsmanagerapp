DROP TABLE "NEWS_AUTHORS";
DROP TABLE "NEWS_TAGS";
DROP TABLE "COMMENTS";
DROP TABLE "TAGS";
DROP TABLE "AUTHORS";
DROP TABLE "ROLES";
DROP TABLE "USERS";

DROP SEQUENCE "NWS_SEQ";
CREATE SEQUENCE "NWS_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;

DROP SEQUENCE "COM_SEQ";
CREATE SEQUENCE "COM_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE;

DROP SEQUENCE "AUT_SEQ";
CREATE SEQUENCE  "AUT_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

DROP SEQUENCE "ROL_SEQ";
CREATE SEQUENCE  "ROL_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

DROP SEQUENCE "TG_SEQ";
CREATE SEQUENCE  "TG_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

DROP SEQUENCE "USR_SEQ";
CREATE SEQUENCE  "USR_SEQ"  MINVALUE 1 MAXVALUE 999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

CREATE TABLE  "NEWS"
(	"NWS_ID" NUMBER(20) NOT NULL ENABLE,
"NWS_TITLE" VARCHAR2(30) NOT NULL ENABLE,
"NWS_SHORT_TEXT" VARCHAR2(100) NOT NULL ENABLE,
"NWS_FULL_TEXT" VARCHAR2(2000) NOT NULL ENABLE,
"NWS_CREATION_DATE" TIMESTAMP (6) NOT NULL ENABLE,
"NWS_MODIFICATION_DATE" DATE NOT NULL ENABLE,
CONSTRAINT "NWS_PK" PRIMARY KEY ("NWS_ID") ENABLE
);

CREATE OR REPLACE TRIGGER  "BI_NEWS"
before insert on "NEWS"
for each row
begin
  select "NWS_SEQ".nextval into :NEW.NWS_ID from dual;
end;

CREATE TABLE  "COMMENTS"
(	"COM_ID" NUMBER(20) NOT NULL ENABLE,
"COM_NEWS_ID" NUMBER(20) NOT NULL ENABLE,
"COM_TEXT" VARCHAR2(100) NOT NULL ENABLE,
"COM_CREATION_DATE" TIMESTAMP (6) NOT NULL ENABLE,
CONSTRAINT "COM_PK" PRIMARY KEY ("COM_ID") ENABLE,
CONSTRAINT "COMMENTS_FK" FOREIGN KEY ("COM_NEWS_ID")
REFERENCES  "NEWS" ("NWS_ID") ENABLE
);

CREATE OR REPLACE TRIGGER  "BI_COMMENTS"
before insert on "COMMENTS"
for each row
begin
  select "COM_SEQ".nextval into :NEW.COM_ID from dual;
end;

CREATE TABLE  "AUTHORS"
(	"AUT_ID" NUMBER(20),
   "AUT_NAME" VARCHAR2(30) NOT NULL ENABLE,
"AUT_EXPIRED" TIMESTAMP (6),
CONSTRAINT "AUTHORS_PK" PRIMARY KEY ("AUT_ID") ENABLE
);

CREATE OR REPLACE TRIGGER  "BI_AUTHORS"
before insert on "AUTHORS"
for each row
begin
  select "AUT_SEQ".nextval into :NEW.AUT_ID from dual;
end;

CREATE TABLE  "NEWS_AUTHORS"
(	"NWA_NEWS_ID" NUMBER NOT NULL ENABLE,
"NWA_AUTHOR_ID" NUMBER(20) NOT NULL ENABLE,
CONSTRAINT "NWA_NEWS_FK" FOREIGN KEY ("NWA_NEWS_ID")
REFERENCES  "NEWS" ("NWS_ID") ENABLE,
CONSTRAINT "NEWS_AUTHORS_FK" FOREIGN KEY ("NWA_AUTHOR_ID")
REFERENCES  "AUTHORS" ("AUT_ID") ENABLE
);

CREATE TABLE  "TAGS"
(	"TG_ID" NUMBER(20) NOT NULL ENABLE,
"TG_NAME" VARCHAR2(30) NOT NULL ENABLE,
CONSTRAINT "TG_PK" PRIMARY KEY ("TG_ID") ENABLE
);

CREATE OR REPLACE TRIGGER  "BI_TAGS"
before insert on "TAGS"
for each row
begin
  select "TG_SEQ".nextval into :NEW.TG_ID from dual;
end;

CREATE TABLE  "NEWS_TAGS"
(	"NWT_NEWS_ID" NUMBER(20) NOT NULL ENABLE,
"NWT_TAG_ID" NUMBER(20) NOT NULL ENABLE,
CONSTRAINT "NWT_NEWS_FK" FOREIGN KEY ("NWT_NEWS_ID")
REFERENCES  "NEWS" ("NWS_ID") ENABLE,
CONSTRAINT "NWT_TAG_FK" FOREIGN KEY ("NWT_TAG_ID")
REFERENCES  "TAGS" ("TG_ID") ENABLE
);

CREATE TABLE  "USERS"
(	"USR_ID" NUMBER(20) NOT NULL ENABLE,
"USR_NAME" VARCHAR2(50) NOT NULL ENABLE,
"USR_LOGIN" VARCHAR2(30) NOT NULL ENABLE,
"USR_PASSWORD" VARCHAR2(30) NOT NULL ENABLE,
CONSTRAINT "USR_PK" PRIMARY KEY ("USR_ID") ENABLE
);

CREATE OR REPLACE TRIGGER  "BI_USERS"
before insert on "USERS"
for each row
begin
  select "USR_SEQ".nextval into :NEW.USR_ID from dual;
end;

CREATE TABLE  "ROLES"
(	"ROL_ID" NUMBER(20) NOT NULL ENABLE,
"ROL_NAME" VARCHAR2(50) NOT NULL ENABLE,
"ROL_USER_ID" NUMBER NOT NULL ENABLE,
CONSTRAINT "ROL_PK" PRIMARY KEY ("ROL_ID") ENABLE,
CONSTRAINT "ROL_FK" FOREIGN KEY ("ROL_USER_ID")
REFERENCES  "USERS" ("USR_ID") ENABLE
);

CREATE OR REPLACE TRIGGER  "BI_ROLES"
before insert on "ROLES"
for each row
begin
  select "ROL_SEQ".nextval into :NEW.ROL_ID from dual;
end;



